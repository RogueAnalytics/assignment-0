/***********************************************************
* Author: W. Aaron Morris
* Date Created: 1/5/2019
* Last Modification Date: 1/5/2019
* Filename: Assignment 0
*
* Overview:
* This program will calculate the area of a triangle using Heron's formula.
*
* Input:
* The input will consist of the length of three sides of a triangle.
*
* Example input file contents:
* 2 3 4
*
* Output:
* The output of this program will be area of the triangle.
*
* Example output contents:
* 3.904738
*
************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// This function calculates the area of a triangle
double triangleArea(double side1, double side2, double side3);

int main(int argc, char **argv) {
    printf("The area of the triangle is %lf", triangleArea(atof(argv[1]), atof(argv[2]), atof(argv[3])));
}

/**************************************************************
* Entry: Three sides of a triangle
*
* Exit: Area of a triangle
*
* Purpose: Calculate the area of a triangle given three sides.
*
***************************************************************/
double triangleArea(double side1, double side2, double side3){
    double sp = (side1 + side2 + side3)/2;
    double area = sqrt(sp * (sp - side1) * (sp - side2) * (sp - side3));
    return area;
}
